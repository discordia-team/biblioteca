/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Diego
 */
public class PessoaEmprestimo extends Emprestimo{
    
    private int codAluno;
    private int codFunc;
    private String dataEmprestimo;
    private String dataEntrega;

    public PessoaEmprestimo() {
    }


    public PessoaEmprestimo(int codAluno, int codFunc, String dataEmprestimo, String dataEntrega, int codigo) {
        super(codigo);
        this.codAluno = codAluno;
        this.codFunc = codFunc;
        this.dataEmprestimo = dataEmprestimo;
        this.dataEntrega = dataEntrega;
    }

    public int getCodAluno() {
        return codAluno;
    }

    public int getCodFunc() {
        return codFunc;
    }

    public String getDataEmprestimo() {
        return dataEmprestimo;
    }

    public String getDataEntrega() {
        return dataEntrega;
    }

    public void setCodAluno(int codAluno) {
        this.codAluno = codAluno;
    }

    public void setCodFunc(int codFunc) {
        this.codFunc = codFunc;
    }

    public void setDataEmprestimo(String dataEmprestimo) {
        this.dataEmprestimo = dataEmprestimo;
    }

    public void setDataEntrega(String dataEntrega) {
        this.dataEntrega = dataEntrega;
    }
    
    
    
    
}
