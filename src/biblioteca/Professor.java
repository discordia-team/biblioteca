/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Diego
 */
public class Professor extends Pessoa {
    private String setor;

    public Professor(){
        super(0, "", "", "");
        this.setor = "";
    }
    public Professor(String setor, int matricula, String nome, String end, String dataEntrada) {
        super(matricula, nome, end, dataEntrada);
        this.setor = setor;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    @Override
    public String toString() {
        return super.toString()+"Professor{" + "setor=" + setor + '}';
    }
    
    
}
