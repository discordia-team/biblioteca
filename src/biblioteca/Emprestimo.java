/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Diego
 */
public class Emprestimo {

    private int codigo;

    public Emprestimo() {
        codigo = 0;
    }

    public Emprestimo(int codigo) {
        this.codigo = codigo;
    }
    
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Emprestimo{" + "codigo=" + codigo + '}';
    }

}
