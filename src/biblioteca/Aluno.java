/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Diego
 */
public class Aluno extends Pessoa {
    
    private String curso;
    private double multa;

    public Aluno() {
        super(0,"","","");
        this.curso = "";
        this.multa = 0;
    }
    public Aluno(String curso, double multa, int matricula, String nome, String end, String dataEntrada) {
        super(matricula, nome, end, dataEntrada);
        this.curso = curso;
        this.multa = multa;
    }

    public String getCurso() {
        return curso;
    }

    public double getMulta() {
        return multa;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public void setMulta(double multa) {
        this.multa = multa;
    }

    @Override
    public String toString() {
        return super.toString()+ "Aluno{" + "curso=" + curso + ", multa=" + multa + '}';
    }
    
   
    
}
