/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Diego
 */
public abstract class Pessoa {
    private int matricula;
    private String nome;
    private String end;
    private String dataEntrada;

    public Pessoa(int matricula, String nome, String end, String dataEntrada) {
        this.matricula = matricula;
        this.nome = nome;
        this.end = end;
        this.dataEntrada = dataEntrada;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public void setDataEntrada(String dataEntrada) {
        this.dataEntrada = dataEntrada;
    }
    
    

    public int getMatricula() {
        return matricula;
    }

    public String getNome() {
        return nome;
    }

    public String getEnd() {
        return end;
    }

    public String getDataEntrada() {
        return dataEntrada;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "matricula=" + matricula + ", nome=" + nome + ", end=" + end + ", dataEntrada=" + dataEntrada + '}';
    }
    
    
    
}
