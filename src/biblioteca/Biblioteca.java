/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/*
↓↓↓↓↓↓---LEIAM ESSA MERDA AQUI---↓↓↓↓↓↓↓

Olhem a pasta library dentro do arquivo, deixei a API de imprimir PDF nela
só tem que mudar os dados que estão dentro do PDF pelos dados 
Olhem a função gerarRelatorio lá no final


é necessário adicionar o itextpdf-5.5.13.jar que está dentro da pasta library

↑↑↑↑↑↑--- LEIAM ESSA MERDA AQUI---↑↑↑↑↑↑
 */
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 *
 * @author Diego
 */
public class Biblioteca {
    
    static boolean login = false; //variavel que verifica o login de usuario
    static Aluno aluno;
    static Professor professor;
    static Funcionario funcionario;
    static Livro livro;
    static Periodico periodico;
    static Emprestimo emprestimo;
    static String pathRoot = "data";//local dos arquivos dentro da pasta data na raiz do programa
    /* nomeArquivo vetor com os nomes de todos os arquivos */
    static String nomeArquivo[] = {"funcionarios", "alunos", "professores", "livros", "periodicos", "emprestimos", "itemEmprestimos"};
    /* arqHeader vetor com os cabeçarios de todos os arquivos */
    static String arqHeader[] = {"Matrícula;Nome;Endereço;data-ingresso;Setor;Senha;Login",
        "Matrícula;Nome;Endereço;Curso;data-ingresso;Multa",
        "Matrícula;Nome;Endereço;data-ingresso;Setor",
        "Código;Autor(es);Título;Editora;Tipo;Ano de Publicação;ISSN;Disponível",
        "Código;Autor(es);Título;Tipo;Fator-de-Impacto;ISSN;Disponível",
        "código;matrícula-cliente;matrícula-funcionário;data-empréstimo;data-devolução",
        "código-item;código-empréstimo;código-livro;código-periódico;data-devolução"
    };
    static String extensao = ".csv";//extensão do arquivo
static String pastaRelatorio= pathRoot+"\\"relatorio;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);//variavel scanner
        byte respostaMenu;//variavel que recebe a resposta do menu
        boolean programa = true;//variavel para manter o programa rodando
        String lixo;//string para receber o lixo acumulado no scan

        do {//inicio do do que roda todo o programa
            if (login) {//verifica se tem usuario logado
                do {//inicio do do que roda o programa após usuario logado
                    linha();
                    imprimir("Bem Vindo! " + funcionario.getNome());//mensagem de boas vindas
                    menuMain();//imprime na tela mensagem de boas vindas
                    respostaMenu = scan.nextByte();//resposta do menu
                    lixo = scan.nextLine();//variavel lixo recebendo o lixo so scanner

                    switch (respostaMenu) {//inicio do switch com a resposta do menu
                        case 1://case 1 cadastro de itens
                            linha();
                            menuCadastroItem();//imprime o menu de itens 1:livro 2:periodicos
                            respostaMenu = scan.nextByte();//resposta do menu
                            lixo = scan.nextLine();//variavel lixo recebendo o lixo so scanner
                            do {//inicio do do com o submenu para cadastro de itens
                                switch (respostaMenu) {//inicio do switch com a resposta do menu
                                    case 1:
                                        cadastrarLivro(); //cadastra livro
                                        programa = false; //variavel programa recebe false para sair do do
                                        break;
                                    case 2:
                                        cadastrarPeriodico();//cadastra periodico
                                        programa = false;//variavel programa recebe false para sair do do
                                        break;
                                    case 9://opção 9 para voltar para o menu anterior
                                        programa = false;//variavel programa recebe false para sair do do
                                        break;
                                    default://caso a escolha do usuário não exista no menu
                                        imprimir("Escolha apensa as opções a baixo");
                                        break;

                                }//fim do switch com a resposta do menu
                            } while (programa);//fim do do com o submenu para cadastro de itens
                            programa = true;

                            break;
                        case 2:
                            linha();
                            menuCadastroUsuario();//imprime o menu de itens 1:Alunos 2:Professores 3:Usuario
                            respostaMenu = scan.nextByte();//resposta do menu
                            lixo = scan.nextLine();//variavel lixo recebendo o lixo so scanner

                            do {//inicio do do com o submenu para cadastro de usuarios
                                switch (respostaMenu) {//inicio do switch com a resposta do menu
                                    case 1:
                                        cadastrarAluno();//cadastro de alunos
                                        programa = false;//variavel programa recebe false para sair do do
                                        break;
                                    case 2:
                                        cadastrarProfessor();//cadastro de Professores
                                        programa = false;//variavel programa recebe false para sair do do
                                        break;
                                    case 3:
                                        cadastrarUsuario();//cadastro de Usuario
                                        programa = false;//variavel programa recebe false para sair do do
                                        break;
                                    case 9://opção 9 para voltar para o menu anterior
                                        programa = false;//variavel programa recebe false para sair do do
                                        break;
                                    default://caso a escolha do usuário não exista no menu
                                        imprimir("Escolha apensa as opções a baixo");
                                        break;

                                }//fim do switch com a resposta do menu
                            } while (programa);//fim do do com o submenu para cadastro de usuarios
                            programa = true;//variavel programa recebe true novamente para não sair do do seguinte

                            break;
                        case 3:
                            linha();
                            emprestimoExemplar();//empretimo de livro (falta concluir)

                            break;
                        case 4://emissão de relatório( falta concluir)
                            linha();
                            gerarRelatorio();
                            break;
                        case 5://cadastro de devoluções( falta concluir)
                            linha();
                            devolucao();
                            break;
                        case 9://opção 9 para voltar para o menu anterior
                            programa = false;
                            break;
                        default://caso a escolha do usuário não exista no menu
                            imprimir("Escolha apensa as opções a baixo");
                            break;

                    }
                } while (programa);//fim do do que roda o programa após usuario logado
            } else {
                //caso usuario não esteja logado

                /*
                o programa verifica todos as pastas e os arquivos necessários
                caso esteja faltando alguma pasta ele cria a pasta e o arquivo
                caso falte apenas o arquivo ele cria
                 */
                verificarArquivos();
                menuLoginCadastro();//imprime o menu de login/cadastro
                respostaMenu = scan.nextByte();//resposta do menu
                lixo = scan.nextLine();//variavel lixo recebendo o lixo so scanner
                switch (respostaMenu) {//inicio do do com o submenu para login/cadastro
                    case 1:
                        cadastrarUsuario();//cadastra novo funcionário
                        break;
                    case 2:
                        login();//faz login
                        break;
                    case 9://opção 9 para voltar para o menu anterior
                        programa = false;
                        break;
                    default://caso a escolha do usuário não exista no menu
                        imprimir("Escolha apensa as opções a baixo");
                        break;

                }//fim do do com o submenu para login/cadastro
            }

        } while (programa);//fim do do que roda todo o programa
    }

    public static void verificarArquivos() {//verifica as pastas e arquivos
        Path path = Paths.get(pathRoot);
        if (!Files.exists(path)) {//verifica a pasta raiz (data)
            criaDiretorios(pathRoot);
        } else {
            for (int i = 0; i < nomeArquivo.length; i++) {
                verificarDiretorio(pathRoot + "\\" + nomeArquivo[i]);//verifica as demais pastas
            }
            for (int i = 0; i < nomeArquivo.length; i++) {
                verificarArquivos(pathRoot + "\\" + nomeArquivo[i] + "\\" + nomeArquivo[i] + extensao, i);//verifica os arquivos
            }
        }
    }

    public static void verificarDiretorio(String arquivo) {//se pasta não existe ela é criada
        Path path = Paths.get(arquivo);
        if (!Files.exists(path)) {
            criaDiretorios(arquivo);
        }
    }

    public static void verificarArquivos(String arquivo, int position) {//se arquivo não existe ele é criado
        Path path = Paths.get(arquivo);
        if (!Files.exists(path)) {
            criaArquivo(arquivo, arqHeader[position], true);
        }
    }

    public static void criaDiretorios(String path) {//cria as pastas
        try {
            File diretorio = new File(path);
            diretorio.mkdir();

        } catch (Exception ex) {
            imprimir("Erro ao criar o diretorio");
            System.out.println(ex);
        }
    }

    public static void criaArquivo(String nomeArq, String dado, boolean manter) {//cria os arquivos

        FileWriter arq;
        try {
            arq = new FileWriter(nomeArq, manter);
            PrintWriter gravarArq = new PrintWriter(arq);
            gravarArq.println(dado);
            arq.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static int pegaQtdRegistros(String nomeArq) {//pega a quantidade de linhas no arquivo para icrementar o código e matricula
        try {
            int num = 0;
            FileReader arq = new FileReader(nomeArq);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // l� a primeira linha
            while (linha != null) {
                linha = lerArq.readLine(); // l� da segunda at� a �ltima linha
                num += 1;
            }
            arq.close();
            return num;

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
        return 0;
    }

    public static void cadastrarLivro() {// cadastra livros
        Scanner scan = new Scanner(System.in);
        String autor, titulo, editora, tipo, dado, novaMatricula, lixo;
        Livro livro = new Livro();
        int matricula, ano, issn, copias;
        linha();
        imprimir("Autor: \n ~>");
        livro.setAutor(scan.nextLine());
        imprimir("Título: \n ~>");
        livro.setTitulo(scan.nextLine());
        imprimir("Editora: \n ~>");
        livro.setEditora(scan.nextLine());
        livro.setTipo('L');
        imprimir("Ano de publicação: \n ~>");
        livro.setDataPublicacao(scan.nextLine());
        imprimir("ISSN: \n ~>");
        livro.setIssn(scan.nextLine());
        imprimir("Números de Cópias:");
        copias = scan.nextInt();
        livro.setDisponivel('S');
        livro.setCodigo(pegaQtdRegistros("data\\livros\\livros" + extensao));

        for (int i = 0; i < copias; i++) {

            dado = livro.getCodigo() + ";"
                    + livro.getAutor() + ";"
                    + livro.getTitulo() + ";"
                    + livro.getEditora() + ";"
                    + livro.getTipo() + ";"
                    + livro.getDataPublicacao() + ";"
                    + livro.getIssn() + ";"
                    + livro.getDisponivel();
            criaArquivo("data\\livros\\livros" + extensao, dado, true);
            livro.setCodigo(livro.getCodigo() + 1);
        }

    }

    public static void cadastrarPeriodico() {//cadastra periodicos
        Scanner scan = new Scanner(System.in);
        String dado, lixo;
        Periodico periodico = new Periodico();
        int matricula, copias;
        linha();
        imprimir("Autor: \n ~>");
        periodico.setAutor(scan.nextLine());
        imprimir("Título: \n ~>");
        periodico.setTitulo(scan.nextLine());

        periodico.setTipo('P');
        imprimir("Fator de impacto: \n->");
        periodico.setFatorDeImpacto(scan.nextInt());
        lixo = scan.nextLine();
        imprimir("ISSN: \n->");
        periodico.setIssn(scan.nextLine());
        imprimir("Números de Cópias:");
        copias = scan.nextInt();
        periodico.setDisponivel('S');
        periodico.setCodigo(pegaQtdRegistros("data\\periodicos\\periodicos" + extensao));

        for (int i = 0; i < copias; i++) {

            dado = periodico.getCodigo() + ";"
                    + periodico.getAutor() + ";"
                    + periodico.getTitulo() + ";"
                    + periodico.getTipo() + ";"
                    + periodico.getFatorDeImpacto() + ";"
                    + periodico.getIssn() + ";"
                    + periodico.getDisponivel();
            criaArquivo("data\\periodicos\\periodicos" + extensao, dado, true);
            periodico.setCodigo(periodico.getCodigo() + 1);
        }

    }

    public static void cadastrarUsuario() {//cadastra funcionários

        Scanner scan = new Scanner(System.in);
        Funcionario funcionario = new Funcionario();
        String dado;
        int matricula, dia, mes, ano;
        linha();
        imprimir("Nome: \n ~>");
        funcionario.setNome(scan.nextLine());
        imprimir("Endereço: \n ~>");
        funcionario.setEnd(scan.nextLine());
        imprimir("Setor: \n ~>");
        funcionario.setSetor(scan.nextLine());
        imprimir("login: \n ~>");
        funcionario.setLogin(scan.nextLine());
        imprimir("senha: \n ~>");
        funcionario.setPassword(scan.nextLine());
        imprimir("Data do Dia ingresso: \n ~>");
        dia = scan.nextInt();
        imprimir("Data do Mês ingresso: \n ~>");
        mes = scan.nextInt();
        imprimir("Data do Ano ingresso: \n ~>");
        ano = scan.nextInt();
        funcionario.setDataEntrada(dia + "/" + mes + "/" + ano);
        funcionario.setMatricula(pegaQtdRegistros("data\\funcionarios\\funcionarios" + extensao));

        dado = funcionario.getMatricula() + ";"
                + funcionario.getNome() + ";"
                + funcionario.getEnd() + ";"
                + funcionario.getDataEntrada() + ";"
                + funcionario.getSetor() + ";"
                + funcionario.getPassword() + ";"
                + funcionario.getLogin();

        criaArquivo("data\\funcionarios\\funcionarios" + extensao, dado, true);

    }

    public static void cadastrarAluno() {//cadastra alunos

        Scanner scan = new Scanner(System.in);
        Aluno Aluno = new Aluno();
        String dado;
        int matricula, dia, mes, ano;

        imprimir("Nome: \n ~>");
        Aluno.setNome(scan.nextLine());
        imprimir("Endereço: \n ~>");
        Aluno.setEnd(scan.nextLine());
        imprimir("Curso: \n ~>");
        Aluno.setCurso(scan.nextLine());
        imprimir("Data do Dia ingresso: \n ~>");
        dia = scan.nextInt();
        imprimir("Data do Mês ingresso: \n ~>");
        mes = scan.nextInt();
        imprimir("Data do Ano ingresso: \n ~>");
        ano = scan.nextInt();
        Aluno.setDataEntrada(dia + "/" + mes + "/" + ano);
        Aluno.setMatricula(pegaQtdRegistros("data\\alunos\\alunos" + extensao));

        dado = Aluno.getMatricula() + ";"
                + Aluno.getNome() + ";"
                + Aluno.getEnd() + ";"
                + Aluno.getCurso() + ";"
                + Aluno.getDataEntrada() + ";"
                + "0";

        criaArquivo("data\\alunos\\alunos" + extensao, dado, true);
    }

    public static void cadastrarProfessor() {//cadastra professores

        Scanner scan = new Scanner(System.in);
        Professor professor = new Professor();
        String dado;
        int matricula, dia, mes, ano;

        imprimir("Nome: \n ~>");
        professor.setNome(scan.nextLine());
        imprimir("Endereço: \n ~>");
        professor.setEnd(scan.nextLine());
        imprimir("Setor: \n ~>");
        professor.setSetor(scan.nextLine());
        imprimir("Data do Dia ingresso: \n~>");
        dia = scan.nextInt();
        imprimir("Data do Mês ingresso: \n~>");
        mes = scan.nextInt();
        imprimir("Data do Ano ingresso: \n~>");
        ano = scan.nextInt();
        professor.setDataEntrada(dia + "/" + mes + "/" + ano);
        professor.setMatricula(pegaQtdRegistros("data\\professores\\professores" + extensao));

        dado = professor.getMatricula() + ";"
                + professor.getNome() + ";"
                + professor.getEnd() + ";"
                + professor.getDataEntrada() + ";"
                + professor.getSetor() + ";";

        criaArquivo("data\\professores\\professores" + extensao, dado, true);
    }

    /*
    
    formatarMatricula não está mais sendo usado talvez nem seja necessário
    
     */
    public static String formatarMatricula(int matricula, String prefixo) {
        String novaMatricula = "";

        if (matricula < 10) {
            novaMatricula = prefixo + "0000000" + matricula;
        }
        if (matricula < 100) {
            novaMatricula = prefixo + "000000" + matricula;
        }
        if (matricula < 1000) {
            novaMatricula = prefixo + "00000" + matricula;
        }
        if (matricula < 10000) {
            novaMatricula = prefixo + "0000" + matricula;
        }
        if (matricula < 100000) {
            novaMatricula = prefixo + "000" + matricula;
        }
        if (matricula < 1000000) {
            novaMatricula = prefixo + "00" + matricula;
        }
        if (matricula < 10000000) {
            novaMatricula = prefixo + "0" + matricula;
        }
        if (matricula < 10000000) {
            novaMatricula = prefixo + matricula;
        }

        return novaMatricula;
    }

    public static boolean checarMulta(int matricula) {
        try {
            String dados[];
            int mat = 0;
            dados = new String[7];
            FileReader arq = new FileReader("data\\alunos\\alunos" + extensao);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // l� a primeira linha
            while (linha != null) {

                linha = lerArq.readLine(); // l� da segunda at� a �ltima linha
                dados = linha.split(";");
                mat = Integer.parseInt(dados[0]);

                if (matricula == mat) {

                    if (dados[5].equalsIgnoreCase("0")) {
                        return true;
                    } else {
                        return false;
                    }

                }
            }

            if (!login) {
                imprimir("Usuário ou senha inválidos!");
            }

            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }

        return false;
    }

    public static void imprimeDadosAlunos(int mat) {
        try {
            String dados[] = new String[7];
            int matricula = 0;
            FileReader arq = new FileReader("data\\alunos\\alunos" + extensao);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // primeira linha cabeçalho
            linha = lerArq.readLine(); // segunda linha em diante

            while (linha != null) {

                dados = linha.split(";");
                matricula = Integer.parseInt(dados[0]);
                if (mat == matricula) {
                    imprimir("Matricula: " + dados[0]);
                    imprimir("Nome: " + dados[1]);
                    imprimir("Endereço: " + dados[2]);
                    imprimir("Curso: " + dados[3]);
                    imprimir("Multa: " + dados[5]);
                }

                linha = lerArq.readLine(); // l� da segunda at� a �ltima linha

            }

            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
    }

    public static void imprimeDadosLivros(int cod) {
        try {
            String dados[] = new String[7];
            int codigo = 0;
            FileReader arq = new FileReader("data\\livros\\livros" + extensao);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // primeira linha cabeçalho
            linha = lerArq.readLine(); // segunda linha em diante

            while (linha != null) {

                dados = linha.split(";");
                codigo = Integer.parseInt(dados[0]);
                if (cod == codigo) {
                    imprimir("Código: " + dados[0]);
                    imprimir("Autor: " + dados[1]);
                    imprimir("Título: " + dados[2]);
                    imprimir("Editora: " + dados[3]);
                }

                linha = lerArq.readLine(); // l� da segunda at� a �ltima linha

            }

            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
    }

    public static void imprimeDadosPeriodico(int cod) {
        try {
            String dados[] = new String[7];
            int codigo = 0;
            FileReader arq = new FileReader("data\\periodicos\\periodicos" + extensao);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // primeira linha cabeçalho
            linha = lerArq.readLine(); // segunda linha em diante

            while (linha != null) {

                dados = linha.split(";");
                codigo = Integer.parseInt(dados[0]);
                if (cod == codigo) {
                    imprimir("Código: " + dados[0]);
                    imprimir("Autor: " + dados[1]);
                    imprimir("Título: " + dados[2]);
                }

                linha = lerArq.readLine(); // l� da segunda at� a �ltima linha

            }

            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
    }

    public static void mudarStatusLivro(int cod, char mudarPara) {
        try {
            boolean mudanca = false;
            String dados[] = new String[8];
            int qtdLivro = pegaQtdRegistros("data\\livros\\livros" + extensao);
            Livro[] livro = new Livro[qtdLivro - 1];
            int codigo = 0, count = 0;
            FileReader arq = new FileReader("data\\livros\\livros" + extensao);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // primeira linha cabeçalho
            linha = lerArq.readLine(); // segunda linha em diante

            while (linha != null) {

                dados = linha.split(";");
                codigo = Integer.parseInt(dados[0]);

                if (cod == codigo) {
                    dados[7] = String.valueOf(mudarPara);
                    mudanca = true;
                }
                livro[count] = new Livro();
                livro[count].setCodigo(Integer.parseInt(dados[0]));
                livro[count].setAutor(dados[1]);
                livro[count].setTitulo(dados[2]);
                livro[count].setEditora(dados[3]);
                livro[count].setTipo(dados[4].charAt(0));
                livro[count].setDataPublicacao(dados[5]);
                livro[count].setIssn(dados[6]);
                livro[count].setDisponivel(dados[7].charAt(0));
                linha = lerArq.readLine(); // l� da segunda at� a �ltima linha
                count++;
            }
            count = 0;
            if (mudanca) {
                for (int i = 0; i < livro.length; i++) {
                    dados[0] = livro[i].getCodigo() + ";"
                            + livro[i].getAutor() + ";"
                            + livro[i].getTitulo() + ";"
                            + livro[i].getEditora() + ";"
                            + livro[i].getTipo() + ";"
                            + livro[i].getDataPublicacao() + ";"
                            + livro[i].getIssn() + ";"
                            + livro[i].getDisponivel() + ";";

                    if (count == 0) {
                        criaArquivo("data\\livros\\livros" + extensao, arqHeader[3], false);
                        criaArquivo("data\\livros\\livros" + extensao, dados[0], true);
                        count = 1;
                    } else {
                        criaArquivo("data\\livros\\livros" + extensao, dados[0], true);
                    }

                }
            }
            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
    }

    public static void mudarStatusPeriodico(int cod, char mudarPara) {
        try {
            boolean mudanca = false;
            String dados[] = new String[6];
            int qtdPeriodico = pegaQtdRegistros("data\\periodicos\\periodicos" + extensao);
            Periodico[] periodico = new Periodico[qtdPeriodico - 1];
            int codigo = 0, count = 0;
            FileReader arq = new FileReader("data\\periodicos\\periodicos" + extensao);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // primeira linha cabeçalho
            linha = lerArq.readLine(); // segunda linha em diante

            while (linha != null) {

                dados = linha.split(";");
                codigo = Integer.parseInt(dados[0]);

                if (cod == codigo) {
                    dados[6] = String.valueOf(mudarPara);
                    mudanca = true;
                }
                periodico[count] = new Periodico();
                periodico[count].setCodigo(Integer.parseInt(dados[0]));
                periodico[count].setAutor(dados[1]);
                periodico[count].setTitulo(dados[2]);
                periodico[count].setTipo(dados[3].charAt(0));
                periodico[count].setFatorDeImpacto(Double.parseDouble(dados[4]));
                periodico[count].setIssn(dados[5]);
                periodico[count].setDisponivel(dados[6].charAt(0));
                linha = lerArq.readLine(); // l� da segunda at� a �ltima linha
                count++;
            }
            count = 0;
            if (mudanca) {
                for (int i = 0; i < periodico.length; i++) {
                    dados[0] = periodico[i].getCodigo() + ";"
                            + periodico[i].getAutor() + ";"
                            + periodico[i].getTitulo() + ";"
                            + periodico[i].getTipo() + ";"
                            + periodico[i].getFatorDeImpacto() + ";"
                            + periodico[i].getIssn() + ";"
                            + periodico[i].getDisponivel() + ";";

                    if (count == 0) {
                        criaArquivo("data\\periodicos\\periodicos" + extensao, arqHeader[4], false);
                        criaArquivo("data\\periodicos\\periodicos" + extensao, dados[0], true);
                        count = 1;
                    } else {
                        criaArquivo("data\\periodicos\\periodicos" + extensao, dados[0], true);
                    }

                }
            }
            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
    }

    public static boolean disponibilidadeExemplar(String arquivo, int cod, int tamanhoVetor) {

        try {
            String dados[] = new String[tamanhoVetor];
            int codigo = 0;
            FileReader arq = new FileReader(arquivo);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // primeira linha cabeçalho
            linha = lerArq.readLine(); // segunda linha em diante

            while (linha != null) {

                dados = linha.split(";");
                codigo = Integer.parseInt(dados[0]);

                if (cod == codigo && dados[tamanhoVetor - 1].equalsIgnoreCase("S")) {
                    return true;
                }

                linha = lerArq.readLine(); // segunda linha em diante
            }
            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
        return false;
    }

    public static void verificarDataMulta(String dataDevolucao, int cod) {
        String dataA = pegarDataAtual();
        String dataDvet[] = new String[3];
        String dataAvet[] = new String[3];

        dataDvet = dataDevolucao.split("/");
        dataAvet = dataA.split("/");
        int diaD, diaA, mesD, mesA, anoD, anoA, dias = 0, diasAtraso = 0, multa = 0;

        diaD = Integer.parseInt(dataDvet[0]);
        diaA = Integer.parseInt(dataAvet[0]);
        mesD = Integer.parseInt(dataDvet[1]);
        mesA = Integer.parseInt(dataAvet[1]);
        anoD = Integer.parseInt(dataDvet[2]);
        anoA = Integer.parseInt(dataAvet[2]);

        int mes[] = new int[12];

        mes[0] = 31;

        mes[2] = 31;
        mes[3] = 30;
        mes[4] = 31;
        mes[5] = 30;
        mes[6] = 31;
        mes[7] = 31;
        mes[8] = 30;
        mes[9] = 31;
        mes[10] = 30;
        mes[11] = 31;

        if (anoA % 4 == 0) {
            if (anoA % 100 == 0) {
                //não é bissexto
                mes[1] = 28;
            } else {
                //é bissexto
                mes[1] = 29;
            }
        } else {
            if (anoA % 400 == 0) {
                //é bissexto
                mes[1] = 29;
            } else {
                //não é bissexto
                mes[1] = 28;

            }
        }

        if (anoD >= anoA) {
            if (mesD >= mesA) {
                if (diaD >= diaA) {
                } else {
                    diasAtraso = diaA - diaD;
                }
            } else {
                if (diaD <= diaA) {
                } else {
                    diasAtraso = diaA - (diaD - 31);
                }
            }
        } else {
            if (mesD < mesA) {
                if (diaD <= diaA) {
                } else {
                    diasAtraso = diaA - diaD;
                }
            } else {

                if (diaD < diaA) {
                } else {
                    for (int i = 0; i < mesA; i++) {
                        dias += mes[i];
                        System.out.println(dias);
                    }
                    diasAtraso = (diaD - diaA + dias);
                }
            }
        }
        for (int i = 0; i < diasAtraso; i++) {
            multa += 1;
        }

        if (diasAtraso != 0) {
            System.out.println("Você está entregando com " + diasAtraso + " dias de atraso\n"
                    + "o que gera uma multa de R$" + multa + "\n "
                    + "(1 Real para cada dia de Atraso)");
            inserirMultaArquivo(cod, multa);
        }
    }

    public static void inserirMultaArquivo(int codAluno, double multa) {
        try {
            boolean mudanca = false;
            String dados[] = new String[6];
            int qtdPeriodico = pegaQtdRegistros("data\\alunos\\alunos" + extensao);
            Aluno[] aluno = new Aluno[qtdPeriodico - 1];
            int codigo = 0, count = 0;
            FileReader arq = new FileReader("data\\alunos\\alunos" + extensao);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // primeira linha cabeçalho
            linha = lerArq.readLine(); // segunda linha em diante

            while (linha != null) {

                dados = linha.split(";");
                codigo = Integer.parseInt(dados[0]);

                if (codAluno == codigo) {
                    dados[5] = Double.toHexString(multa);
                    mudanca = true;
                }
                aluno[count] = new Aluno();
                aluno[count].setMatricula(Integer.parseInt(dados[0]));
                aluno[count].setNome(dados[1]);
                aluno[count].setEnd(dados[2]);
                aluno[count].setCurso(dados[3]);
                aluno[count].setDataEntrada(dados[4]);
                aluno[count].setMulta(Double.parseDouble(dados[5]));
                linha = lerArq.readLine(); // l� da segunda at� a �ltima linha
                count++;
            }
            count = 0;
            if (mudanca) {
                for (int i = 0; i < aluno.length; i++) {
                    dados[0] = aluno[i].getMatricula() + ";"
                            + aluno[i].getNome() + ";"
                            + aluno[i].getEnd() + ";"
                            + aluno[i].getCurso() + ";"
                            + aluno[i].getDataEntrada() + ";"
                            + aluno[i].getMulta() + ";";

                    if (count == 0) {
                        criaArquivo("data\\alunos\\alunos" + extensao, arqHeader[1], false);
                        criaArquivo("data\\alunos\\alunos" + extensao, dados[0], true);
                        count = 1;
                    } else {
                        criaArquivo("data\\alunos\\alunos" + extensao, dados[0], true);
                    }

                }
            }
            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
    }

    public static void gerarMulta(int codEmprestimo) {
        try {
            String dados[] = new String[5];
            int codigo = 0;
            FileReader arq = new FileReader("data\\emprestimos\\emprestimos" + extensao);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // primeira linha cabeçalho
            linha = lerArq.readLine(); // segunda linha em diante

            while (linha != null) {

                dados = linha.split(";");
                codigo = Integer.parseInt(dados[0]);
                if (codigo == codEmprestimo) {

                    verificarDataMulta(dados[4], Integer.parseInt(dados[1]));

                }

                linha = lerArq.readLine(); // segunda linha em diante
            }
            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
    }

    public static void mudarDataDevolucaoExemplar(int codItem, int item) {// 2 livro 3 periodico
        try {
            boolean mudanca = false;
            String dados[] = new String[5];
            String data = pegarDataAtual();
            int qtdPeriodico = pegaQtdRegistros("data\\itemEmprestimos\\itemEmprestimos" + extensao);
            ItemEmprestimo[] itemEmprestimo = new ItemEmprestimo[qtdPeriodico - 1];
            int codigo = 0, count = 0;
            FileReader arq = new FileReader("data\\itemEmprestimos\\itemEmprestimos" + extensao);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // primeira linha cabeçalho
            linha = lerArq.readLine(); // segunda linha em diante

            while (linha != null) {

                dados = linha.split(";");
                codigo = Integer.parseInt(dados[0]);

                if (codItem == Integer.parseInt(dados[item]) && dados[4].equalsIgnoreCase("em Aberto")) {
                    dados[4] = data;

                    gerarMulta(Integer.parseInt(dados[1]));

                    mudanca = true;
                }
                itemEmprestimo[count] = new ItemEmprestimo();
                itemEmprestimo[count].setCodigoItem(Integer.parseInt(dados[0]));
                itemEmprestimo[count].setCodigo(Integer.parseInt(dados[1]));
                itemEmprestimo[count].setCodLivro(Integer.parseInt(dados[2]));
                itemEmprestimo[count].setCodPeriodico(Integer.parseInt(dados[3]));
                itemEmprestimo[count].setDataDevolucao(dados[4]);
                linha = lerArq.readLine(); // l� da segunda at� a �ltima linha
                count++;
            }
            count = 0;
            if (mudanca) {
                for (int i = 0; i < itemEmprestimo.length; i++) {
                    dados[0] = itemEmprestimo[i].getCodigoItem() + ";"
                            + itemEmprestimo[i].getCodigo() + ";"
                            + itemEmprestimo[i].getCodLivro() + ";"
                            + itemEmprestimo[i].getCodPeriodico() + ";"
                            + itemEmprestimo[i].getDataDevolucao() + ";";

                    if (count == 0) {
                        criaArquivo("data\\itemEmprestimos\\itemEmprestimos" + extensao, arqHeader[6], false);
                        criaArquivo("data\\itemEmprestimos\\itemEmprestimos" + extensao, dados[0], true);
                        count = 1;
                    } else {
                        criaArquivo("data\\itemEmprestimos\\itemEmprestimos" + extensao, dados[0], true);
                    }

                }
            }
            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
    }

    public static String pegarDataAtual() {
        Date data = new Date();
        SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
        return formatador.format(data);
    }

    public static String pegarDataDevolucao() {
        String data[] = new String[3];
        data = pegarDataAtual().split("/");
        String dataDevolucao = "", dataAtual;
        int dia = Integer.parseInt(data[0]), mes = Integer.parseInt(data[1]), ano = Integer.parseInt(data[2]);

        switch (mes) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:

                dia += 5;
                if (dia > 31) {
                    mes += 1;
                    dia -= 31;
                    if (mes > 12) {
                        mes = 1;
                        dia = 1;
                        ano += 1;
                    }
                }

                break;

            case 4:
            case 6:
            case 9:
            case 11:
                dia += 5;
                if (dia > 30) {
                    mes += 1;
                    dia -= 31;
                    if (mes > 12) {
                        mes = 1;
                        ano += 1;
                    }
                }
                break;

            case 2:

                if (ano % 4 == 0) {
                    if (ano % 100 == 0) {
                        //não é bissexto
                        dia += 5;
                        if (dia > 28) {
                            mes += 1;
                            dia -= 31;
                            if (mes > 12) {
                                mes = 1;
                                ano += 1;
                            }
                        }
                    } else {
                        //é bissexto
                        dia += 5;
                        if (dia > 29) {
                            mes += 1;
                            dia -= 31;
                            if (mes > 12) {
                                mes = 1;
                                ano += 1;
                            }
                        }
                    }
                } else {
                    if (ano % 400 == 0) {
                        //é bissexto
                        dia += 5;
                        if (dia > 29) {
                            mes += 1;
                            dia -= 31;
                            if (mes > 12) {
                                mes = 1;
                                ano += 1;
                            }
                        }
                    } else {
                        //não é bissexto
                        dia += 5;
                        if (dia > 28) {
                            mes += 1;
                            dia -= 31;
                            if (mes > 12) {
                                mes = 1;
                                ano += 1;
                            }
                        }
                    }
                }

                break;
        }
        dataDevolucao = dia + "/" + mes + "/" + ano;
        return dataDevolucao;
    }

    public static void emprestimoExemplar() {
        char resposta1;
        boolean livroDisponivel = false, periodicoDisponivel = false;
        ItemEmprestimo itemEmprestimo = new ItemEmprestimo();
        PessoaEmprestimo pessoaEmprestimo = new PessoaEmprestimo();
        Scanner scan = new Scanner(System.in);
        int cod;
        byte resposta;
        String lixo, dado, data, dataDevolucao;
        boolean programa = true;
        do {
            imprimir("Matricula: \n ~>");
            pessoaEmprestimo.setCodAluno(scan.nextInt());
            imprimeDadosAlunos(pessoaEmprestimo.getCodAluno());
            imprimir("Está Correto? (S):Sim/(N)Não");
            System.out.print("~>");
            resposta1 = scan.next().charAt(0);

            if (resposta1 == 's' || resposta1 == 'S') {
                programa = false;
            }
        } while (programa);
        programa = true;
        if (checarMulta(pessoaEmprestimo.getCodAluno())) {

            pessoaEmprestimo.setCodFunc(funcionario.getMatricula());
            do {
                imprimir("[1] ~> Livros: ");
                imprimir("[2] ~> Periódicos: ");
                resposta = scan.nextByte();
                lixo = scan.nextLine();

                switch (resposta) {

                    case 1:
                        do {
                            imprimir("Código do Livro: \n->");
                            itemEmprestimo.setCodLivro(scan.nextInt());
                            imprimeDadosLivros(itemEmprestimo.getCodLivro());
                            if (disponibilidadeExemplar("data\\livros\\livros" + extensao, itemEmprestimo.getCodLivro(), 8)) {
                                itemEmprestimo.setCodPeriodico(0);
                                imprimir("Está Correto? (S):Sim/(N)Não");
                                System.out.print("~>");
                                resposta1 = scan.next().charAt(0);

                                if (resposta1 == 's' || resposta1 == 'S') {
                                    programa = false;
                                }

                                livroDisponivel = true;
                            } else {
                                imprimir("Desculpe mas o livro informado não está disponível");

                                /*
                                
                                adicionar uma forma de consultar livros disponíveis aqui
                                
                                 */
                            }

                        } while (programa);

                        if (livroDisponivel) {
                            mudarStatusLivro(itemEmprestimo.getCodLivro(), 'N');
                        }

                        break;

                    case 2:

                        do {
                            imprimir("Código do Periódico: \n->");
                            itemEmprestimo.setCodPeriodico(scan.nextInt());
                            lixo = scan.nextLine();
                            imprimeDadosPeriodico(itemEmprestimo.getCodPeriodico());
                            if (disponibilidadeExemplar("data\\periodicos\\periodicos" + extensao, itemEmprestimo.getCodPeriodico(), 7)) {
                                itemEmprestimo.setCodLivro(0);
                                imprimir("Está Correto? (S):Sim/(N)Não");
                                System.out.print("~>");
                                resposta1 = scan.next().charAt(0);

                                if (resposta1 == 's' || resposta1 == 'S') {
                                    programa = false;
                                }

                                periodicoDisponivel = true;
                            } else {
                                imprimir("Desculpe mas o periódico informado não está disponível");

                                /*
                                
                                adicionar uma forma de consultar periodicos disponíveis aqui
                                
                                 */
                            }

                        } while (programa);

                        if (periodicoDisponivel) {
                            mudarStatusPeriodico(itemEmprestimo.getCodPeriodico(), 'N');
                        }

                        break;

                }

            } while (programa);

            data = pegarDataAtual();
            dataDevolucao = pegarDataDevolucao();
            pessoaEmprestimo.setDataEmprestimo(data);
            itemEmprestimo.setDataDevolucao("em Aberto");
            pessoaEmprestimo.setDataEntrega(dataDevolucao);
            cod = pegaQtdRegistros("data\\emprestimos\\emprestimos" + extensao);
            itemEmprestimo.setCodigo(cod);
            pessoaEmprestimo.setCodigo(cod);
            cod = pegaQtdRegistros("data\\itemEmprestimos\\itemEmprestimos" + extensao);
            itemEmprestimo.setCodigoItem(cod);

            dado = itemEmprestimo.getCodigoItem() + ";"
                    + itemEmprestimo.getCodigo() + ";"
                    + itemEmprestimo.getCodLivro() + ";"
                    + itemEmprestimo.getCodPeriodico() + ";"
                    + itemEmprestimo.getDataDevolucao() + ";";

            criaArquivo("data\\itemEmprestimos\\itemEmprestimos" + extensao, dado, true);

            dado = pessoaEmprestimo.getCodigo() + ";"
                    + pessoaEmprestimo.getCodAluno() + ";"
                    + pessoaEmprestimo.getCodFunc() + ";"
                    + pessoaEmprestimo.getDataEmprestimo() + ";"
                    + pessoaEmprestimo.getDataEntrega() + ";";

            criaArquivo("data\\emprestimos\\emprestimos" + extensao, dado, true);

            imprimir("data do emprestimo: " + data);
            imprimir("data para devolução: " + dataDevolucao);
            imprimir("Emprestimo realizado com sucesso!");

        } else {
            imprimir("Aluno possui multas pendentes!");
        }

    }

    public static void devolucao() {

        char resposta1;
        boolean livroDisponivel = false, periodicoDisponivel = false;
        ItemEmprestimo itemEmprestimo = new ItemEmprestimo();
        PessoaEmprestimo pessoaEmprestimo = new PessoaEmprestimo();
        Scanner scan = new Scanner(System.in);
        int cod;
        byte resposta;
        String lixo, dado, data, dataDevolucao;
        boolean programa = true;

        do {
            imprimir("[1] ~> Livros: ");
            imprimir("[2] ~> Periódicos: ");
            resposta = scan.nextByte();
            lixo = scan.nextLine();

            switch (resposta) {

                case 1:
                    do {
                        imprimir("Código do Livro: \n->");
                        itemEmprestimo.setCodLivro(scan.nextInt());
                        imprimeDadosLivros(itemEmprestimo.getCodLivro());
                        imprimir("Está Correto? (S):Sim/(N)Não");
                        System.out.print("~>");
                        resposta1 = scan.next().charAt(0);

                        if (resposta1 == 's' || resposta1 == 'S') {
                            programa = false;
                        }

                    } while (programa);

                    mudarStatusLivro(itemEmprestimo.getCodLivro(), 'S');
                    mudarDataDevolucaoExemplar(itemEmprestimo.getCodLivro(), 2);
                    break;

                case 2:

                    do {
                        imprimir("Código do Periódico: \n->");
                        itemEmprestimo.setCodPeriodico(scan.nextInt());
                        lixo = scan.nextLine();
                        imprimeDadosPeriodico(itemEmprestimo.getCodPeriodico());
                        imprimir("Está Correto? (S):Sim/(N)Não");
                        System.out.print("~>");
                        resposta1 = scan.next().charAt(0);

                        if (resposta1 == 's' || resposta1 == 'S') {
                            programa = false;
                        }

                    } while (programa);

                    mudarStatusPeriodico(itemEmprestimo.getCodPeriodico(), 'S');
                    mudarDataDevolucaoExemplar(itemEmprestimo.getCodPeriodico(), 3);

                    break;

            }

        } while (programa);

        imprimir("Devolução realizada com sucesso!");

    }

    public static void login() {//login
        String user, password;
        Scanner scan = new Scanner(System.in);
        imprimir("Login: ");
        user = scan.nextLine();

        imprimir("Senha: ");
        password = scan.nextLine();
        loginCheck(user, password);

    }

    public static void loginCheck(String usr, String pass) {//faz a comparação do login e senha com as armazenadas no arquivo
        try {
            String dados[] = new String[7];
            int matricula = 0;
            FileReader arq = new FileReader("data\\funcionarios\\funcionarios" + extensao);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine(); // primeira linha cabeçalho
            linha = lerArq.readLine(); // segunda linha em diante
            while (linha != null) {

                dados = linha.split(";");

                if (usr.equalsIgnoreCase(dados[6]) && pass.equalsIgnoreCase(dados[5])) {
                    matricula = Integer.parseInt(dados[0]);
                    funcionario = new Funcionario(matricula, dados[1], dados[2], dados[3], dados[4], dados[5], dados[6]);
                    login = true;
                }
                linha = lerArq.readLine(); // l� da segunda at� a �ltima linha

            }

            if (!login) {
                imprimir("Usuário ou senha inválidos!");
            }

            arq.close();

        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }
    }

    public static void menuMain() {//menu principal
        imprimir("[1] ~> Cadastro de itens: ");
        imprimir("[2] ~> Cadastro de usuários: ");
        imprimir("[3] ~> Empréstimo");
        imprimir("[4] ~> Emitir Relatório");
        imprimir("[5] ~> Devolução de itens");
        imprimir("[9] ~> Sair");
        System.out.print("Opção ~> ");
    }

    public static void menuCadastroItem() {//menu itens
        imprimir("[1] ~> Cadastro de Livros: ");
        imprimir("[2] ~> Cadastro de Periódicos: ");
        imprimir("[9] ~> Voltar");
        System.out.print("Opção ~> ");
    }

    public static void menuCadastroUsuario() {//menu usuario
        imprimir("[1] ~> Cadastro de Aluno: ");
        imprimir("[2] ~> Cadastro de Professores: ");
        imprimir("[3] ~> Cadastro de Funcionários: ");
        imprimir("[9] ~> Voltar");
        System.out.print("Opção ~>  ");
    }

    public static void menuEmprestimos() {//menu emprestimos
        imprimir("[1] ~> Livros: ");
        imprimir("[2] ~> Periódicos: ");
        imprimir("[9] ~> Voltar");
        System.out.print("Opção ~> ");
    }

    public static void menuLoginCadastro() {//menu login/cadastro
        imprimir("[1] ~> Cadastrar Novo Usuario");
        imprimir("[2] ~> Fazer Login");
        imprimir("[9] ~> Sair");
        System.out.print("Opção ~> ");
    }

    public static void imprimir(String string) {//prequiça de digitar System.out.println
        System.out.println(string);
    }

    public static void linha() {
        imprimir("###########################################################################");
    }

      public static void gerarRelatorio(){
                        
            try {
                   verificarDiretorio(pathRoot);                
                   for (int j = 0; j < nomeArquivo.length; j++) {
                        verificarDiretorio(pathRoot + "\\" + nomeArquivo[j]);
                   verificarArquivos(pathRoot + "\\" + nomeArquivo[j] + "\\" + nomeArquivo[j] + extensao, j);
                   
                String arquivoName = pathRoot + "\\" + nomeArquivo[j] + "\\" + nomeArquivo[j] + extensao;             
                    System.out.println(arquivoName);
                    FileReader arquivo = new FileReader(arquivoName);
                    
                    BufferedReader buffer = new BufferedReader(arquivo);
                    
                    while (true) {
                         String linha = buffer.readLine();
                               if(linha != null){
                                   System.out.println(nomeArquivo[j]+": \n");         
                         System.out.println(linha);
                               }
                        if (linha == null) {
                            System.out.println("\n");
                            break;
                        }
                    }
                   }
            

            } catch (IOException e) {
               

            }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
     

}
