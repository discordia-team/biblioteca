/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Diego
 */
public class Livro extends Exemplares{
    private String editora;
    private String dataPublicacao;

    public Livro(){
        super(0, "","", 'L', "");
        this.editora = editora;
        this.dataPublicacao = dataPublicacao;
    }
    
    public Livro(String editora, String dataPublicacao, int codigo, String autor, String titulo, char tipo, String issn) {
        super(codigo, autor, titulo, tipo, issn);
        this.editora = editora;
        this.dataPublicacao = dataPublicacao;
    }

    public String getEditora() {
        return editora;
    }

    public String getDataPublicacao() {
        return dataPublicacao;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public void setDataPublicacao(String dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    @Override
    public String toString() {
        return super.toString()+"Livro{" + "editora=" + editora + ", dataPublicacao=" + dataPublicacao + '}';
    }
    
}
