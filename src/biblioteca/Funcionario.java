/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Diego
 */
public class Funcionario extends Pessoa {

    private String setor;
    private String login;
    private String password;

    public Funcionario() {
        super(0, "", "", "");
        this.setor = "";
    }

    public Funcionario(int matricula, String nome, String end, String dataEntrada,String setor,String password,String login) {
        super(matricula, nome, end, dataEntrada);
        this.setor = setor;
        this.password = password;
        this.login = login;
    }

    public String getSetor() {
        return setor;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return super.toString() + "Funcionario{" + "setor=" + setor + ", login=" + login + ", password=" + password + '}';
    }

}
