/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Diego
 */
public class ItemEmprestimo extends Emprestimo{
    
    private int codigoItem;
    private int codLivro;
    private int codPeriodico;
    private String dataDevolucao;

    public ItemEmprestimo() {
    }

    public ItemEmprestimo(int codigo, int codigoItem, int codLivro, int codPeriodico, String dataDevolução) {
        super(codigo);
        this.codigoItem = codigoItem;
        this.codLivro = codLivro;
        this.codPeriodico = codPeriodico;
        this.dataDevolucao = dataDevolução;
    }

    public int getCodigoItem() {
        return codigoItem;
    }

    public int getCodLivro() {
        return codLivro;
    }

    public int getCodPeriodico() {
        return codPeriodico;
    }

    public String getDataDevolucao() {
        return dataDevolucao;
    }

    public void setCodigoItem(int codigoItem) {
        this.codigoItem = codigoItem;
    }

    public void setCodLivro(int codLivro) {
        this.codLivro = codLivro;
    }

    public void setCodPeriodico(int codPeriodico) {
        this.codPeriodico = codPeriodico;
    }

    public void setDataDevolucao(String dataDevolução) {
        this.dataDevolucao = dataDevolução;
    }

    @Override
    public String toString() {
        return "ItemEmprestimo{" + "codigoItem=" + codigoItem + ", codLivro=" + codLivro + ", codPeriodico=" + codPeriodico + ", dataDevolu\u00e7\u00e3o=" + dataDevolucao + '}';
    }

    

    
}
