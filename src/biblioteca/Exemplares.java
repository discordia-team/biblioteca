/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Diego
 */
public abstract class Exemplares {
    private int codigo;
    private String autor;
    private String titulo;
    private char tipo;
    private String issn;
    private char disponivel;

    public Exemplares(int codigo, String autor, String titulo, char tipo, String issn) {
        this.codigo = codigo;
        this.autor = autor;
        this.titulo = titulo;
        this.tipo = tipo;
        this.issn = issn;
        this.disponivel = 'S';
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public void setDisponivel(char disponivel) {
        this.disponivel = disponivel;
    }
    
    

    public int getCodigo() {
        return codigo;
    }

    public String getAutor() {
        return autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public char getTipo() {
        return tipo;
    }

    public String getIssn() {
        return issn;
    }

    public char getDisponivel() {
        return disponivel;
    }

    @Override
    public String toString() {
        return "Exemplares{" + "codigo=" + codigo + ", autor=" + autor + ", titulo=" + titulo + ", tipo=" + tipo + ", issn=" + issn + ", disponivel=" + disponivel + '}';
    }

    
    
    
    
    
    
}
