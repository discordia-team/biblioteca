/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Diego
 */
public class Periodico extends Exemplares {
    private double fatorDeImpacto;

     public Periodico() {
        super(0, "", "", 'P', "");
        this.fatorDeImpacto = 0;
    }
    
    public Periodico(double fatorDeImpacto, int codigo, String autor, String titulo, char tipo, String issn) {
        super(codigo, autor, titulo, tipo, issn);
        this.fatorDeImpacto = fatorDeImpacto;
    }

    public double getFatorDeImpacto() {
        return fatorDeImpacto;
    }

    public void setFatorDeImpacto(double fatorDeImpacto) {
        this.fatorDeImpacto = fatorDeImpacto;
    }

    @Override
    public String toString() {
        return super.toString()+"Periodico{" + "fatorDeImpacto=" + fatorDeImpacto + '}';
    }
    
    
    
}
